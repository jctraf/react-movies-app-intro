import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return(
      // PART 1: Rendering Simple Components
      // -----------------------------------------
      // EXERCISE 1: Write the code to render a <h1>, <h2> containing the name and genre of a movie (Star Wars / Sci Fi)
      
      // EXERCISE 2: Abstract your html into a reusable "Movie" component. 
      // This means: calling <Movie> should render:
      // <h1> Star Wars: The Last Jedi </h1>
      // <h2> Sci Fi </h2> 
      // Test your <Movie> element by calling it in your App.js file.

      // EXERCISE 3: Update your "Movie" component to accept a user-provided movie name and movie genre. 
      // The movie name and genre are passed into the <Movie> element using attributes (props)
      // Test your component by making 3 different <Movie> elements in your App.js file

      <h1>HELLO WORLD</h1> 
    )
  }
}

export default App;
